use <encoder.scad>

carrier_wall_w = 4;
carrier_r = encoder_r() + carrier_wall_w;
carrier_h = encoder_h();

inf = 25;
module encoder_carrier() {
    difference() {
    cylinder(carrier_h, carrier_r, carrier_r);
    
    encoder();
    
    translate([0, 0, encoder_h() - carrier_wall_w])
    for(i = [0: 120: 360])
        rotate([0, 0, i + encoder_screw_pos_ang()])
        translate([encoder_screw_pos_r(), 0, 0])
        cylinder(inf, encoder_screw_r(), 
            encoder_screw_r(), true);
    };
}