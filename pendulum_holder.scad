use <encoder.scad>

$fn = 100;
arm_cap_h = encoder_shaft_h();
arm_cap_r = 9;

cut_w = arm_cap_r / 7;
cut_screw_r = 2.1 / 2 + 0.3;
cut_washer_r = 5.45 / 2;

side_screw_r = 5.1 / 2;

pendulum_r = 5;
pendulum_case_wall_w = 1.5;
pendulum_case_r = pendulum_r + pendulum_case_wall_w;

pendulum_cut_w = pendulum_r / 7;
pendulum_clamp_w = pendulum_case_wall_w;

module pendulum_transition(height) {
    hull() {
        rotate([0, 90, 0])
        translate([-pendulum_case_r, 0, 0])
        difference() {
            cylinder(height, pendulum_case_r, pendulum_case_r);
            translate([pendulum_case_r / 1.3, 0, height - pendulum_cut_w])
            cube([pendulum_case_r, pendulum_case_r * 2,  height - pendulum_cut_w], true);
        }
    }
}

module pendulum_case(height) {
    rotate([0, 90, 0])
    difference() {
        union() {
            cylinder(height, pendulum_case_r, pendulum_case_r);
            translate([cut_washer_r * 2.4, 0, height / 2])
            cube([pendulum_case_r, 2 * pendulum_clamp_w + pendulum_cut_w, height], true);
        }
        
        cylinder(height, pendulum_r, pendulum_r);
        
        translate([pendulum_r * 2, 0, height / 2])
        cube([pendulum_case_r * 2, pendulum_cut_w, height], true);
        
        translate([2 * pendulum_case_r + cut_screw_r, 0, 0])
        for(i = [1.5/9, 4.5/9, 7.5/9])
            translate([-5 * arm_cap_r / 7, arm_cap_r, height * i])
            rotate([90, 0, 0])
            cylinder(arm_cap_r * 2, cut_screw_r, cut_screw_r);
    }
}

module cap() {
    difference() {
            union() {
            hull() {
                cylinder(arm_cap_h, arm_cap_r, arm_cap_r);
                pendulum_transition(arm_cap_r);
            }
            pendulum_transition(arm_cap_r * 1.1);
        }
    
        scale([1, 1, 2])
        difference() {
            cylinder(arm_cap_h, arm_cap_r * 4 / 7, arm_cap_r * 4 / 7);
            cylinder(arm_cap_h, arm_cap_r * 3.5 / 7, arm_cap_r * 3 / 7);
            
            translate([arm_cap_r / 5, -arm_cap_r, 0])
            cube([arm_cap_r * 2, arm_cap_r * 2, arm_cap_h]);
            
            translate([-arm_cap_r / 2, 0, arm_cap_h / 2])
            cube([arm_cap_r, arm_cap_r * 3 / 7 - encoder_shaft_r() +
                cut_w * 2, arm_cap_h], true);
        }
    
        encoder_shaft();
        
        scale([1, 1, 2])
        translate([-arm_cap_r, -cut_w / 2, 0])
        cube([arm_cap_r, cut_w, arm_cap_h]);
        
        for(i = [2.5/9, 6.6/9])
            translate([-5 * arm_cap_r / 7, arm_cap_r, arm_cap_h * i * 0.9])
            rotate([90, 0, 0])
            cylinder(arm_cap_r * 2, cut_screw_r, cut_screw_r);
        
        for(i = [1, -1])
            translate([- arm_cap_r * 1, -arm_cap_r * i * 7/8, arm_cap_h * 0.9 / 2])
            cube([arm_cap_r, arm_cap_r, arm_cap_h * 0.9], true);
    }
}

cap();

translate([arm_cap_r + arm_cap_r * 0.1, 0, pendulum_case_r])
pendulum_case(20);