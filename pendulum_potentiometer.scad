use <potentiometer.scad>
use <encoder.scad>
use <screw.scad>

$fn = 200;
connector_r = 10;
push_h = 4.5;
push_r = 2;
thread_w = 10;

/*
difference() {
    cylinder(potentiometer_shaft_h() * 1.2, connector_r, connector_r);
    
    translate([-connector_r - push_h, 0, potentiometer_shaft_h() / 2])
    rotate([0, -90, 180])
    screw(0, 0, connector_r, thread_w, push_h / 2, push_r);
    
    potentiometer_shaft();
}


translate([0, 0, potentiometer_shaft_h() * 1.2])
scale([0.92, 0.92, 1])
encoder_shaft();*/

    screw(5, thread_w, connector_r / 2, thread_w * 0.92, push_h / 2, push_r);
