use <screw.scad>
use <nema23.scad>
use <potentiometer.scad>
use <potentiometer_holder.scad>

$fn = 200;

thread_w = 15;

arm_cap_h = 23;
arm_cap_r = 13;

push_r = 2.5;
push_h = 4.5;

arm_l = 60;
arm_w = arm_cap_r * 2;
arm_h = arm_w / 2;

arm_wall = (arm_l - potentiometer_h() / 2) / 8.5;

cable_holder_r = 4.4 / 2 + 0.6;
cable_holder_h = 5;
cable_holder_wall_w = 1.5;

hex_h = 4;

module motor_cap() {
    translate([0, 0, arm_cap_h])
    rotate([0, 180, 0])
    difference() {
        cylinder(arm_cap_h, arm_cap_r, arm_cap_r);
        
        rotate([0, 0, 180])
        nema23_shaft();
       
        translate([-arm_cap_r - push_h, 0, arm_cap_h / 2])
        rotate([0, -90, 180])
        screw(0, 0, arm_cap_r, thread_w, push_h / 2, push_r);
    }
}

module motor_cap_screw() {
    screw(hex_h, thread_w, arm_cap_r - nema23_shaft_r(), 
            thread_w * 0.92, push_h * 0.65, push_r * 0.95);
}

module cable_holder() {
    difference() {
        cylinder(cable_holder_h, 
            cable_holder_r + cable_holder_wall_w, 
            cable_holder_r + cable_holder_wall_w);
        
        cylinder(cable_holder_h, cable_holder_r, cable_holder_r);
    }
}

module arm() {    
    difference() {
        translate([0, arm_w / 2, 0])
        rotate([0, 0, 180])
        
        difference() {
            cube([arm_l + potentiometer_h(), arm_w, arm_h]);
            
            for(i = [2, 6, 10, 11])
                translate([i * arm_wall, arm_wall, 0])
                cube([(arm_l + potentiometer_h() / 2 - 2 * arm_wall) / 3.5, 
                    arm_w - 2 * arm_wall, arm_h]);
            
           translate([arm_wall, arm_wall, arm_h / 1.5])
            cube([arm_l + potentiometer_h() / 2 - 2 * arm_wall, 
                arm_w - 2 * arm_wall, arm_h / 1.5]);
        }
        
        cylinder(arm_cap_h, arm_cap_r, arm_cap_r);
    }
}

motor_cap();
arm();

translate([-arm_l, 0, potentiometer_r() + arm_h])
rotate([180, 90, 0])
potentiometer_carrier();

/*
translate([0, arm_w * 1.5, 0])
motor_cap_screw(); */