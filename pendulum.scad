use <encoder.scad>
use <screw.scad>

$fn = 200;

pendulum_h = 150;
pendulum_r = 6;

encoder_cap_r = 11;
encoder_cap_h = 12;

push_r = 2.5;
push_h = 4.5;

hex_h = 4;
thread_w = 10;

module encoder_cap() {
    translate([0, 0, encoder_cap_h])
    rotate([0, 180, 0])
    difference() {
        cylinder(encoder_cap_h, encoder_cap_r, encoder_cap_r);
        
        rotate([0, 0, 180])
        encoder_shaft();
       
        translate([-encoder_cap_r - push_h, 0, encoder_cap_h / 2])
        rotate([0, -90, 180])
        screw(0, 0, encoder_cap_r, thread_w, push_h / 2, push_r);
    }
}

module encoder_cap_screw() {
    screw(hex_h, thread_w, encoder_cap_r - encoder_shaft_r(), 
            thread_w * 0.92, push_h * 0.65, push_r * 0.95);
}


cylinder(pendulum_h - encoder_cap_r / 3, pendulum_r, pendulum_r);

translate([encoder_cap_h / 2, 0, pendulum_h])
rotate([0, -90, 0])
encoder_cap();

/*
translate([2 * encoder_cap_r, 0, 0])
encoder_cap_screw();*/