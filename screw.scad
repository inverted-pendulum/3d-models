use <threads.scad>

screw_pitch = 1.4;

module hex(side, height) {
    hull() {
        for(i = [0: 60: 360])
            rotate([0, 0, i + 45])
            cube([side / 2 / sqrt(2), side / 2 / sqrt(2), height]);
    };
}

module screw(hex_h, hex_s, screw_h, screw_w, pusher_h, pusher_r) {
    assert(screw_w > 5, "too thin screw");
    hex(hex_s, hex_h);
    translate([0, 0, hex_h])
    metric_thread(screw_w, screw_pitch, screw_h);
    translate([0, 0, hex_h + screw_h])
    cylinder(pusher_h, pusher_r, pusher_r);
};