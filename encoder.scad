encoder_r = 38.4 / 2 + 0.5; // TODO update
encoder_h = 34.2;

top_r = 20 / 2 + 0.3; // TODO update
top_h = 39.4 - encoder_h;

shaft_r = 6 / 2 + 0.2; // TODO update
shaft_h = 10; // TODO update
shaft_base_h = 3;
shaft_cut = shaft_r - 5.5 / 2;

cable_w = 22;
cable_h = 18;

screw_r = 2.2;
screw_pos_r = 14.9;
screw_pos_ang = 45;

inf = 25;

module encoder_shaft() {
    translate([0, 0, shaft_base_h])
    difference() {
        cylinder(shaft_h, shaft_r, shaft_r);
        
        translate([shaft_r - shaft_cut, -inf / 2, 0])
        cube([inf, inf, inf]);
    }
    cylinder(shaft_base_h, shaft_r, shaft_r);
}

module encoder() {
    cylinder(encoder_h, encoder_r, encoder_r);
    
    translate([0, 0, encoder_h])
    cylinder(top_h, top_r, top_r);
    
    translate([encoder_r, 0, cable_w / 2])
    rotate([0, 90, 0])
    cube([cable_w, cable_w, cable_h], true); 
}

function encoder_h() = encoder_h + top_h;
function encoder_r() = encoder_r;

function encoder_screw_r() = screw_r;
function encoder_screw_pos_r() = screw_pos_r;
function encoder_screw_pos_ang() = screw_pos_ang;

function encoder_shaft_r() = shaft_r;
function encoder_shaft_h() = shaft_h;