use <potentiometer.scad>
use <screw.scad>

carrier_wall_w = 10;
carrier_r = potentiometer_r() + carrier_wall_w;
carrier_h = potentiometer_h();

thread_w = 12;
push_r = 2.5;
push_h = 5.5;

inf = 25;
module potentiometer_carrier() {
    difference() {
        cylinder(carrier_h, carrier_r, carrier_r);
        potentiometer();
        
        translate([0, - 3 / 2 * carrier_r - push_h / 2, carrier_h / 2])
        rotate([-90, 0, 0])
        screw(0, 0, carrier_r, thread_w, push_h / 2, push_r);
    }
}

rotate([180, 0, 0])
union() {
    translate([0, 0, carrier_h])
    rotate([180, 0, 0])
    potentiometer_carrier();

    translate([carrier_r * 2, 0, 0])
    screw(5, thread_w, carrier_r / 3, thread_w * 0.92, push_h / 1.3, push_r * 0.95);
}