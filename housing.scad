use <nema23.scad>

$fn = 170;
inf = 300;

driver_w = 96.5 + 0.2;
driver_d = 57.1 + 0.2; // TODO update
driver_h = 27.3 + 0.2; // TODO update

p2_r1 = 1 * driver_w / 2 + 15; // TODO 1.1* !!!
p2_r2 = p2_r1 / 2;
p2_h = 60;

p2_wall_w = 3;

p1_h = 10;
p1_r = p2_r1;

p3_h = 20;
p3_r = p2_r2;
p3_wall_w = p2_wall_w;

p3_clip_w = 3;
p3_clip_h = 4;
p3_clip_r = p3_r - p3_wall_w;
p3_clip_pos_h = 10;

p4_h = 65;
p4_r = p3_r;
p4_wall_w = p3_wall_w;

p4_clip_w = p3_clip_w;
p4_clip_h1 = p3_clip_pos_h;
p4_clip_h2 = p3_clip_h;
p4_clip_r = p4_r - p4_wall_w;

scale = 1.1;

p5_h = 20;
p5_r1 = p4_r;
p5_r2 = nema23_w() * scale * sqrt(2) / 2;
p5_wall_w = p4_wall_w;

p6_bottom_h = 5;
p6_h = nema23_h();
p6_r1 = p5_r2;
p6_r2 = p6_r1 / scale;

leg_w = 40;
leg_d = 20;
leg_h = 20;
leg_curve = 70;

module curved_cube(width, depth, h, curve) {
    height = h / cos(curve);
    difference() {
        rotate([curve, 0, 0])
        cube([width, depth, height * 1.5], true);
        
        for(i = [0: 180: 180])
            rotate([i, 0, 0])
            translate([-inf / 2, - inf / 2, height / 2 
            * cos(curve)])
            cube([inf, inf, inf]);
    }
}

module ring_impl(height, width, r1, r2) {
    difference() {
        cylinder(height, r1, r2);
        cylinder(height, r1 - width, r2 - width);  
    }
}

module ring(height, width, r1) {
    ring_impl(height, width, r1, r1);
}

module half_sphere(radius, width) {
    difference() {
        sphere(radius);
        sphere(radius - width);
        
        translate([-radius, -radius,  -2 * radius])
        cube([2 * radius, 2 * radius, 2 * radius]);
    }
}

module legs() {
    step = 120;
    translate([0, 0, leg_h / 2])
    for(i = [0: step: 360])
        rotate([0, 0, i])
        translate([0, p1_r - leg_d + leg_h * sin(leg_curve), 0])
        curved_cube(leg_w, leg_d, leg_h, leg_curve);
}

module p2() {
    difference() {
        cylinder(p2_h, p2_r1, p2_r2);
        cylinder(p2_h, p2_r1 - p2_wall_w, p2_r2 - p2_wall_w);
        
        hull() {
            scale([driver_w / 2, driver_d * 10, driver_h * 1.3])
            sphere(1);
        
            scale([driver_w / 5, driver_d * 10, driver_h * 2])
            sphere(1);
        }
    }
}

module p1() {
    difference() {
        union() { 
            legs();
            translate([0, 0, leg_h - p1_h])
            cylinder(p1_h, p1_r, p1_r);
        }
        
        translate([0, 0, driver_h * 2 + p1_h * 1.5])
        cube([driver_w, driver_d, driver_h * 4], true);
        
        scale([0.7, 0.7, 0.7])
        cube([driver_w, driver_d, driver_h * 4], true);
    }
}

module p3() {
    ring(p3_h, p3_wall_w, p3_r);
    
    translate([0, 0, p3_h - p3_clip_pos_h - p3_clip_h])
    ring(p3_clip_h, p3_clip_w * 2, p3_clip_r);
    
    translate([0, 0, p3_h - p3_clip_pos_h - p3_clip_h * 2])
    ring(p3_clip_h, p3_clip_w, p3_clip_r);
}

module p4() {
    ring(p4_clip_h1 * 2, p4_clip_w, p4_clip_r);
    
    ring(p4_clip_h2, p4_clip_w * 2, p4_clip_r);
    
    translate([0, 0, p4_clip_h1])
    ring(p4_h, p4_wall_w, p4_r);
}

module p5() {
    difference() {
        cylinder(p5_h, p5_r1, p5_r2);
        cylinder(p5_h, p5_r1, 0);
    }
}

module p6() {
    difference() {        
        union() {
            cylinder(p6_h, p6_r1, p6_r1);
            
            translate([0, 0, p6_h])
            cylinder(p6_bottom_h, p6_r1, p6_r2);
        }
        translate([0, 0, p6_bottom_h + nema23_h() / 2])
        nema23_extended();
    }
}

module clips() {
    p3();
    
 //   translate([0, 0, p3_h - p3_clip_pos_h + 19])
    translate([0, 70, 0])
    p4();
}

module bot() {
    p1();

    translate([0, 0, leg_h])
    p2();

    translate([0, 0, leg_h + p2_h])
    p3();
}

module top() {
    p4();
    
    translate([0, 0, p4_h])
    p5();
    
    translate([0, 0, p4_h + p5_h])
    p6();
}

module all() {
    bot();

    hx = leg_h + p2_h + p3_h - p3_clip_pos_h;
    translate([0, 0, hx])
    top();
}

all();