use <encoder_holder.scad>
use <nema23.scad>
use <encoder.scad>

$fn = 200;

thread_w = 15;

arm_cap_h = 23;
arm_cap_r = 13;

cut_w = arm_cap_r / 7;
cut_screw_r = 2.1 / 2 + 0.3;
cut_washer_r = 5.45 / 2;

push_r = 2.5;
push_h = 4.5;

arm_l = 45;
arm_w = arm_cap_r * 2;
arm_h = arm_w / 2;

arm_wall = (arm_l - encoder_h() / 2) / 8.5;

cable_holder_r = 4.4 / 2 + 0.6;
cable_holder_h = 5;
cable_holder_wall_w = 1.5;

hex_h = 4;

module screw_cut() {
}

module motor_cap() {
    translate([0, 0, arm_cap_h])
    rotate([0, 180, 0])
    difference() {
        cylinder(arm_cap_h, arm_cap_r, arm_cap_r);
        
        difference() {
            cylinder(arm_cap_h, arm_cap_r * 4 / 7, arm_cap_r * 4 / 7);
            cylinder(arm_cap_h, arm_cap_r * 3 / 7, arm_cap_r * 3 / 7);

            translate([arm_cap_r / 5, -arm_cap_r, 0])
            cube([arm_cap_r * 2, arm_cap_r * 2, arm_cap_h]);

            translate([-arm_cap_r / 2, 0, arm_cap_h / 2])
            cube([arm_cap_r, arm_cap_r * 3 / 7 - nema23_shaft_r() + cut_w * 2, arm_cap_h], true);
        }

        scale([1, 1, 2])
        nema23_shaft();

        translate([-arm_cap_r, -cut_w / 2, 0])
        cube([arm_cap_r, cut_w, arm_cap_h]);

        for(i = [2/9, 4.5/9, 7/9])
            translate([-5 * arm_cap_r / 7, arm_cap_r, arm_cap_h * i * 0.9])
            rotate([90, 0, 0])
            cylinder(arm_cap_r * 2, cut_screw_r, cut_screw_r);

        for(i = [1, -1])
            translate([- arm_cap_r * 1, -arm_cap_r * i * 7/8, arm_cap_h * 0.9 / 2])
            cube([arm_cap_r, arm_cap_r, arm_cap_h * 0.9], true);
    }
}

module cable_holder() {
    difference() {
        cylinder(cable_holder_h, 
            cable_holder_r + cable_holder_wall_w, 
            cable_holder_r + cable_holder_wall_w);
        
        cylinder(cable_holder_h, cable_holder_r, cable_holder_r);
    }
}

module arm() {    
    difference() {
        translate([0, arm_w / 2, 0])
        rotate([0, 0, 180])
        
        difference() {
            cube([arm_l + encoder_h(), arm_w, arm_h]);
            
            for(i = [1, 3, 8, 9, 14, 17])
                translate([i * arm_wall, arm_wall, 0])
                cube([(arm_l + encoder_h() / 2 - 2 * arm_wall) / 5,
                    arm_w - 2 * arm_wall, arm_h]);
            
           translate([arm_wall, arm_wall, arm_h / 1.5])
            cube([arm_l + encoder_h() / 2 - 2 * arm_wall, 
                arm_w - 2 * arm_wall, arm_h / 1.5]);
        }
        
        cylinder(arm_cap_h, arm_cap_r, arm_cap_r);
    }
}

motor_cap();

arm();
translate([-arm_l, 0, encoder_r() + arm_h])
rotate([180, 90, 0])
encoder_carrier();
