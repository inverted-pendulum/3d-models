motor_w = 56.3 + 0.2;
motor_d = 56.3 + 0.2;
motor_h = 54.6; // TODO update

motor_cutout_w = 4.45; // TODO update
motor_cutout_top_h = 5.3 + 0.1;
motor_cutout_h = motor_h - motor_cutout_top_h;

shaft_r = 6.4 / 2 + 0.1; // TODO update
shaft_h = 17; // TODO update
shaft_cut = 0.4 + 0.1;

inf = 100;

module nema23_shaft() {
    difference() {
        cylinder(shaft_h, shaft_r, shaft_r);
        
        translate([shaft_r - shaft_cut, -inf / 2, 0])
        cube([inf, inf, inf]);
    }
}

module side_rounded_cube(radius, height, width = inf, depth = inf) {
     cylinder(height, radius, radius);
                
     translate([-radius, 0, 0]) 
     cube([width, depth, height]);
                
     translate([0, -radius, 0])
     cube([width, depth, height]);
}

module nema23() {
    difference() {
        cube([motor_w, motor_d, motor_h], true);
        
        for(i = [0: 90: 270])
            rotate([0, 0, i])
            translate([motor_w / 2 - motor_cutout_w, 
                motor_d / 2 - motor_cutout_w, - motor_h / 2])
            side_rounded_cube(motor_cutout_w, motor_cutout_h);
    };
};

module nema23_extended() {
    nema23();
    
    translate([0, -inf / 2, 0])
    cube([motor_w - motor_cutout_w * 4, inf, motor_h], true);
    
}

function nema23_h() = motor_h;
function nema23_w() = motor_w;
function nema23_d() = motor_d;
function nema23_shaft_h() = shaft_h;
function nema23_shaft_r() = shaft_r;

nema23_shaft();
